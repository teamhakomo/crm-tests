package tests.withoutuser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ui.pages.Texts;
import ui.pages.withoutuser.TermsOfServicePage;
import utils.Browser;


public class TermsOfServicePageTests implements Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void verifyTexts () {
        TermsOfServicePage.open();
        TermsOfServicePage.verifyTextContent(TERMS_OF_SERVING_TEXT);
    }
}
