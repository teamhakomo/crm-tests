package tests.withoutuser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ui.pages.Texts;
import ui.pages.withoutuser.FeaturesPage;
import utils.Browser;


public class FeaturesPageTests implements Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void verifyTexts () {
        FeaturesPage.open();
        FeaturesPage.verifyTextContent(FEATURES_TEXT);
    }
}
