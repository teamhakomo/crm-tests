package tests.withoutuser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ui.pages.Texts;
import ui.pages.withoutuser.PrivacyPolicyPage;
import utils.Browser;

public class PrivacyPolicyPageTests implements Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void verifyTexts () {
        PrivacyPolicyPage.open();
        PrivacyPolicyPage.verifyTextContent(PRIVACY_POLICY_TEXT);
    }
}
