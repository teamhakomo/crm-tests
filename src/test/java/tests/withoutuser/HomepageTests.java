package tests.withoutuser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ui.pages.Texts;
import ui.pages.withoutuser.Homepage;
import utils.Browser;


/**
 * Created by Teodora on 7/3/17.
 */
public class HomepageTests implements Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void openHomePage () {
        Homepage.open();
        Homepage.isAt(HOME_TITLE);
    }

    @Test
    public void openLoginPage () {
        Homepage.open();
        Homepage.loginButton.click();
    }

    @Test
    public void openSignupPageFromNavigation () {
        Homepage.open();
        Homepage.signupNavButton.click();
    }

    @Test
    public void openSignupPage () {
        Homepage.open();
        Homepage.signupButton.click();
    }

    @Test
    public void openFeaturesFromNavigation () {
        Homepage.open();
        Homepage.featuresNavLink.click();
    }

    @Test
    public void openFeaturesFromFooter () {
        Homepage.open();
        Homepage.featuresFooterLink.click();
    }

    @Test
    public void openPricingFromNavigation () {
        Homepage.open();
        Homepage.pricingNavLink.click();
    }

    @Test
    public void openPricingFromFooter () {
        Homepage.open();
        Homepage.pricingFooterLink.click();
    }

    @Test
    public void openTeamPage () {
        Homepage.open();
        Homepage.teamLink.click();
    }

    @Test
    public void openContactsPage () {
        Homepage.open();
        Homepage.contactsLink.click();
    }

    @Test
    public void openFacebook () {
        Homepage.open();
        Homepage.facebookLink.click();
    }

    @Test
    public void openTwitter () {
        Homepage.open();
        Homepage.twitterLink.click();
    }

    @Test
    public void openLinkedin () {
        Homepage.open();
        Homepage.linkedinLink.click();
    }

    @Test
    public void openTermsOfServicePage () {
        Homepage.open();
        Homepage.termsLink.click();
    }

    @Test
    public void openPrivacyPolicyPage () {
        Homepage.open();
        Homepage.privacyLink.click();
    }


    @Test
    public void verifyAllTexts () {
        Homepage.open();
        Homepage.verifyVideoText(HOME_VIDEO_TEXT);
        Homepage.verifyFeatureSectionText(HOME_FEATURES_TEXT);
        Homepage.verifyTestimonialsSectionText(HOME_TESTIMONIALS_TEXT);
        Homepage.verifyMoreInfoSectionText(HOME_MORE_INFO_TEXT);
        Homepage.verifyPMSectionText(HOME_PM_TEXT);
    }


}
