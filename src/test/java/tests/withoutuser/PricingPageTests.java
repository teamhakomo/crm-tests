package tests.withoutuser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ui.pages.Texts;
import ui.pages.withoutuser.PricingPage;
import utils.Browser;

public class PricingPageTests implements Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void verifyText () {
        PricingPage.open();
        PricingPage.verifyTextContent(PRICING_TEXT);
    }

}
