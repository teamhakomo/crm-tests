package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tasks.Login;
import ui.pages.Texts;
import ui.pages.withoutuser.LoginPage;
import utils.Browser;

import static org.junit.Assert.assertEquals;

/**
 * Created by Teodora on 7/4/17.
 */
public class LoginTests implements TestData, Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void loginSuccess () {
        LoginPage.openUrl();
        Login.login(USER_EMAIL, USER_PASS);
        assertEquals("Text doesn't match", LOGIN_HEADING, LoginPage.loginHeading.getText());
    }
}
