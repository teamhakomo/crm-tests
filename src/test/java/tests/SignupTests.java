package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tasks.Signup;
import ui.pages.Texts;
import ui.pages.withoutuser.SignupPage;
import utils.Browser;

public class SignupTests implements TestData, Texts {

    @Before
    public void setUp () {
        Browser.init("chrome");
    }

    @After
    public void tearDown () throws InterruptedException {
        Thread.sleep(1500);
        Browser.driver.quit();
    }

    @Test
    public void signupSuccess () {
        SignupPage.openUrL();
        Signup.signup(SIGNUP_FIRSTNAME, SIGNUP_LASTNAME, SIGNUP_EMAIL, SIGNUP_PASS, SIGNUP_PASS);
    }

}
