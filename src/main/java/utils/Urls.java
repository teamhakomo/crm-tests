package utils;

/**
 * Created by Teodora on 7/3/17.
 */
public interface Urls {
    //Drivers
    String FIREFOX = "/Users/Teodora/www/crmDEV/drivers/geckodriver";
    String CHROME = "/Users/Teodora/www/crmDEV/drivers/chromedriver";

    //Pages
    String ROOT = "localhost:8000";
    String HOMEPAGE = ROOT + "/home";
    String FEATURES = ROOT + "/features";
    String PRICING = ROOT + "/pricing";
    String LOGIN = ROOT + "/accounts/login";
    String SIGNUP = ROOT + "/accounts/signup";
    String TERMS_OF_SERVICES = ROOT + "/terms";
    String PRIVACY_POLICY = ROOT + "/privacy_policy";
    String CONTACTS = "";
    String TEAM = "";
    String FACEBOOK = "https://www.facebook.com/hexakit/";
    String TWITER = "https://twitter.com/usehexakit";
    String LINKEDIN = "https://www.linkedin.com/company/hexakit";

}
