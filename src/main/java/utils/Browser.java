package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Teodora on 7/3/17.
 */
public class Browser implements Urls {
    public static WebDriver driver;
    public static WebDriverWait wait;

    public static void init (String browser) {
        switch (browser) {
            case "chrome":
                initChrome();
                break;
            case "firefox":
                initFirefox();
                break;
            default:
                initChrome();
        }

        wait = new WebDriverWait(driver, 5, 200);

        driver.manage().window().maximize();
    }

    private static void initFirefox () {
        System.setProperty("webdriver.gecko.driver", FIREFOX);
        driver = new FirefoxDriver();
    }

    private static void initChrome () {
        System.setProperty("webdriver.chrome.driver", CHROME);
        driver = new ChromeDriver();
    }

}
