package tasks;

import ui.pages.withoutuser.SignupPage;

/**
 * Created by Teodora on 7/3/17.
 */
public class Signup extends SignupPage {

    public static void populateFirstName (String firstNameParam) {
        firstNameInput.sendKeys(firstNameParam);
    }

    public static void populateLastName (String lastNameParam) {
        lastNameInput.sendKeys(lastNameParam);
    }

    public static void populateEmail (String emailParam) {
        emailInput.sendKeys(emailParam);
    }

    public static void populatePassword (String passwordParam) {
        passInput.sendKeys(passwordParam);
    }

    public static void populateConfirmPass (String confirmPassParam) {
        confirmInput.sendKeys(confirmPassParam);
    }

    public static void clickSignupButton () {
        signupButton.click();
    }

    public static void signup (String firstName, String lastName, String email, String pass, String confirmPass) {
        populateFirstName(firstName);
        populateLastName(lastName);
        populateEmail(email);
        populatePassword(pass);
        populateConfirmPass(confirmPass);
//        clickSignupButton();
    }
}
