package tasks;

import ui.pages.withoutuser.LoginPage;

/**
 * Created by Teodora on 7/3/17.
 */
public class Login extends LoginPage {

    public static void populateEmail (String emailParam) {
        emailInput.sendKeys(emailParam);
    }

    public static void populatePassword (String passParam) {
        passwordInput.sendKeys(passParam);

    }

    public static void clickLoginButton () {
        loginButton.click();

    }

    public static void clickForgotPass () {
        forgotPassLink.click();
    }

    public static void login (String email, String pass) {

        populateEmail(email);
        populatePassword(pass);
        clickLoginButton();

    }


}
