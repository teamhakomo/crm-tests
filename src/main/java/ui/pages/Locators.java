package ui.pages;

/**
 * Created by Teodora on 7/4/17.
 */
public interface Locators {

    //Login Form
    String LOGIN_HEADING = "h3.hk-heading"; //css
    String LOGIN_EMAIL = "id_login"; //id
    String LOGIN_PASSWORD = "id_password"; //id
    String LOGIN_BUTTON = "//button[text()='Log In']"; //xpath
    String FORGOT_PASSWORD = "Forgot Password?"; //linkText

    //Signup Form
    String SIGNUP_FIRSTNAME = "id_first_name"; //id
    String SIGNUP_LASTNAME = "id_last_name";//id
    String SIGNUP_EMAIL = "id_email";//id
    String SIGNUP_PASSWORD = "id_password1";//id
    String SIGNUP_CONFIRMPASS = "id_password2";//id
    String SIGNUP_BUTTON = "//button[text()='Sign Up']"; //xpath

    //Navigation
    String NAV_HOME = "Home"; //linkText
    String NAV_FEATURES = "//a[@class='nav-public-link hk-btn hk-btn__transparent'][text()='Features']"; //xpath
    String NAV_PRICING = "//a[@class='nav-public-link hk-btn hk-btn__transparent'][text()='Pricing']"; //xpath
    String NAV_SIGNUP = "//a[@class='nav-public-link hk-btn hk-btn__dodger-blue']"; //xpath
    String NAV_LOGIN = "Log In"; //linkText
    String NAV_LOGO = "navbar-brand"; //className

    //Homepage
    String HOME_SIGNUP = "//a[@class='hk-btn hk-btn__dodger-blue']";//xpath
    String HOME_VIDEO_SECTION = "video-col-text";//className
    String HOME_FEATURES_SECTION = ".//*[@id='site-content']/div[2]/div[2]/div";//xpath
    String HOME_TESTIMONIALS_SECTION = "section-testimonials-item-text-only";//className
    String HOME_MOREINFO_SECTION = ".container.section-more-info"; //css
    //    String HOME_PM_SECTION = "//div[@class='hexagon-element']"; //xpath
    String HOME_PM_SECTION = "hexagon-element"; //className

    //Footer
    String FOOTER_FEATURES = "//a[@class='footer-link'][text()='Features']"; //xpath
    String FOOTER_PRICING = "//a[@class='footer-link'][text()='Pricing']"; //xpath
    //    String FOOTER_TEAM = "//a[@class='footer-link'][text()='Team']"; //xpath
    String FOOTER_TEAM = "Team"; //linkText
    String FOOTER_CONTACTS = "Contacts"; //linkText
    String FOOTER_FACEBOOK = "Facebook"; //linkText
    String FOOTER_TWITTER = "Twitter"; //linkText
    String FOOTER_LINKEDIN = "LinkedIn"; //linkText
    String FOOTER_TERMS_OF_SERVICE = "Terms of Service"; //linkText
    String FOOTER_PRIVACY_POLICY = "Privacy Policy"; //linkText
    //    String COPYRIGHT = "copyright"; //className
    String FOOTER_LOGO = "";


    //Other
    String FEEDBACK = "feedback"; //id

    //Features Page
    String FEATURES_PAGE_TEXTS = "site-content"; //id
    String FEATURES_PRICING_BUTTON = "See Pricing";//

    //Pricing Page
    String PRICING_SIGNUP_BUTTON = "Sign Up Now";//linkText
    String PRICING_SIGNUP_LINK = "Sign up"; //linkText
    String PRICING_TEXT_PAGE = "//div[@class='container pricing-page']";//xpath

    //Privacy Policy Page
    String PRIVACY_POLICY_HEADING = ".//*[@id='site-content']/div/h2"; //xpath
    String PRIVACY_POLICY_TEXT_PAGE = "site-content";//id
    String PRIVACY_POLICY_HEXAKIT_LINK = "www.hexakit.com";//linkText

    //Terms Of Service Page
    String TERMS_HEADING = ".//*[@id='site-content']/div/h1";//xpath
    String TERMS_TEXT_PAGE = "site-content";//id
    String TERMS_HEXAKIT_LINK = "www.hexakit.com";//linkText

}
