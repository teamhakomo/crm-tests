package ui.pages;

/**
 * Created by Teodora on 7/4/17.
 */
public interface Texts {
    //Login Page
    String LOGIN_HEADING = "LOG IN HERE";

    //Homepage
    String HOME_TITLE = "Homepage | Hexakit";
    String HOME_VIDEO_TEXT = "ALL THE TOOLS YOU NEED TO RUN A DIGITAL AGENCY\n" +
            "\n" +
            "Change requests, bugs tracking, invoices... get all the processes that took us years to build in a snap.";
    String HOME_FEATURES_TEXT = "CHANGE REQUESTS\n" +
            "No more unpaid invoices. Set up your client communication the right way - pre approve every hour of work so clients don't feel cheated and avoid payments.\n" +
            "\n" +
            "LIVE\n" +
            "REPORTING\n" +
            "Create Reports. See the sum of all work. Attach to invoice. Graphs.\n" +
            "\n" +
            "\n" +
            "\n" +
            "LIVE\n" +
            "BUG TRACKING\n" +
            "Get all the required info.No more useless \"X doesn't work\". Clients file a bug with all needed information to track down and squish that bug..\n" +
            "\n" +
            "\n" +
            "SOON";
    String HOME_TESTIMONIALS_TEXT = "PEOPLE DON'T JUST LIKE IT, THEY LOVE IT\n" +
            "Actually, they also need it. Most have said that they can’t live without it anymore!";
    //
    String HOME_MORE_INFO_TEXT = "LEARN FROM THE\n" +
            "DIGITAL AGENCY ACADEMY\n" +
            "We have tons of useful information on how to setup your company processes.\n" +
            "\n" +
            "SOON\n" +
            "\n" +
            "\n" +
            "SEE HOW CHANGE\n" +
            "REQUESTS SAVES MONEY\n" +
            "Case study of how change requests saved us thousands of dollars (Literally!) every month.\n" +
            "\n" +
            "SOON";
    String HOME_PM_TEXT = "BEST PRACTICES IN DIGITAL\n" +
            "AGENCY PROJECT MANAGEMENT\n" +
            "\n" +
            "We will send you a white paper on the best practices of PM. How to be efficient. How to handle clients. How to get paid on those invoices.\n" +
            "\n" +
            "SOON";
//    String HOME_HEADING = "All the tools you need to run a digital agency";
//    String HOME_HEADING = "ALL THE TOOLS YOU NEED TO RUN A DIGITAL AGENCY";
//    String HOME_AFTER_HEADING = "Change requests, bugs tracking, invoices... get all the processes that took us years to build in a snap.";
//    String HOME_CR_HEADING = "CHANGE REQUESTS";
//    String HOME_REPORT_HEADING = "REPORTING";
//    String HOME_BUG_HEADING = "BUG TRACKING";
//    String HOME_AFTER_CR_HEADING = "No more unpaid invoices. Set up your client communication the right way - pre approve every hour of work so clients don't feel cheated and avoid payments.\n";
//    String HOME_AFTER_REPORT_HEADING = "Create Reports. See the sum of all work. Attach to invoice. Graphs.\n";
//    String HOME_AFTER_BUG_HEADING = "Get all the required info.No more useless \"X doesn't work\". Clients file a bug with all needed information to track down and squish that bug..";
//    String HOME_TESTIMONIALS_HEADING = "PEOPLE DON'T JUST LIKE IT, THEY LOVE IT";
//    String HOME_AFTER_TESTIMONIALS_HEADING = "Actually, they also need it. Most have said that they can’t live without it anymore!";
//    String HOME_PM_HEADING = "BEST PRACTICES IN DIGITAL\n" +
//            "AGENCY PROJECT MANAGEMENT";
//    String HOME_AFTER_PM_HEADING = "We will send you a white paper on the best practices of PM. How to be efficient. How to handle clients. How to get paid on those invoices.";


    //Footer
    String COPYRIGHT = "© Copyright 2017 Hexakit. All rights reserved.";

    //Features Page
    String FEATURES_TITLE = "Features | Hexakit";
    String FEATURES_TEXT = "AVOID ARGUING OVER INVOICES WITH CHANGE REQUEST COMPONENT\n" +
            "\n" +
            "Work with pre approved flow\n" +
            "Give estimates in advance. That's right - this means money for you\n" +
            "Discuss before you start working and it is too late.\n" +
            "LIVE\n" +
            "\n" +
            "COMMUNICATE PROGRESS EFFICIENTLY TO CLIENT\n" +
            "\n" +
            "No more flooding of calls & texts from your clients - progress can be seen online\n" +
            "Automate processes, notifications, tasks for both client and employees\n" +
            "No more searching email for version of documents, everything is organized\n" +
            "Clients will love it and start adding more and more things. That's right - this means money for you\n" +
            "LIVE\n" +
            "\n" +
            "KEEP PRIVATE NUMBERS PRIVATE, SEND PUBLIC NUMBERS\n" +
            "\n" +
            "Adjust easily what you see and what your client sees. That's right - this means money for you.\n" +
            "Keep internal statistics internal, but send the client report\n" +
            "All data is saved securely.\n" +
            "SOON\n" +
            "\n" +
            "DISCOVER PROBLEMS AND FIX THEM WITH BUG-TRACKING TOOL\n" +
            "\n" +
            "No more \"nothing is working\" emails. Client has a constructive form to fill\n" +
            "Bugs are stored and prioritized.\n" +
            "Easy transfer from bug to paid change request. That's right - this means money from you.\n" +
            "GET STARTED NOW\n" +
            "\n" +
            "...and save/win money by running the business correctly";

    //Pricing Page
    String PRICING_TITLE = "Pricing | Hexakit";
    String PRICING_TEXT = "PRO\n" +
            "Basic for all professionals\n" +
            "\n" +
            "$5\n" +
            "per person / per month\n" +
            "\n" +
            "\n" +
            "Get paid for invoices\n" +
            "Manage transparently your clients\n" +
            "Keep internal numbers for yourself\n" +
            "\n" +
            "\n" +
            "Sign Up Now\n" +
            "\n" +
            "Hexakit is FREE if you have less than 5 users and 5 active projects. Sign up now.\n" +
            "AWESOME FOR FREELANCERS\n" +
            "PERFECT FOR LARGE AGENCIES";

    //Privacy Policy Page
    String PRIVACY_POLICY_HEADING = "Hexakit Privacy Policy";
    String PRIVACY_POLICY_TEXT = "HEXAKIT PRIVACY POLICY\n" +
            "This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.\n" +
            "WHAT PERSONAL INFORMATION DO WE COLLECT FROM THE PEOPLE THAT VISIT OUR BLOG, WEBSITE OR APP?\n" +
            "When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number or other details to help you with your experience.\n" +
            "WWHEN DO WE COLLECT INFORMATION?\n" +
            "We collect information from you when you register on our site, subscribe to a newsletter, respond to a survey, fill out a form or enter information on our site.\n" +
            "Provide us with feedback on our products or services\n" +
            "HOW DO WE USE YOUR INFORMATION?\n" +
            "We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:\n" +
            "To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.\n" +
            "To improve our website in order to better serve you.\n" +
            "To allow us to better service you in responding to your customer service requests.\n" +
            "To administer a contest, promotion, survey or other site feature.\n" +
            "To quickly process your transactions.\n" +
            "To ask for ratings and reviews of services or products\n" +
            "To follow up with them after correspondence (live chat, email or phone inquiries)\n" +
            "HOW DO WE PROTECT YOUR INFORMATION?\n" +
            "Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.\n" +
            "We use regular Malware Scanning.\n" +
            "Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.\n" +
            "We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.\n" +
            "All transactions are processed through a gateway provider and are not stored or processed on our servers.\n" +
            "DO WE USE 'COOKIES'?\n" +
            "Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.\n" +
            "WE USE COOKIES TO:\n" +
            "Understand and save user's preferences for future visits.\n" +
            "Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.\n" +
            "You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.\n" +
            "IF USERS DISABLE COOKIES IN THEIR BROWSER:\n" +
            "If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.\n" +
            "THIRD-PARTY DISCLOSURE\n" +
            "We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety.\n" +
            "However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.\n" +
            "THIRD-PARTY LINKS\n" +
            "Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.\n" +
            "GOOGLE\n" +
            "Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users.\n" +
            "https://support.google.com/adwordspolicy/answer/1316548?hl=en\n" +
            "We use Google AdSense Advertising on our website.\n" +
            "Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.\n" +
            "WE HAVE IMPLEMENTED THE FOLLOWING:\n" +
            "Google Display Network Impression Reporting\n" +
            "Demographics and Interests Reporting\n" +
            "We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.\n" +
            "OPTING OUT:\n" +
            "Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.\n" +
            "CALIFORNIA ONLINE PRIVACY PROTECTION ACT\n" +
            "CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online- privacy-protection- act- caloppa/#sthash.0FdRbT51.dpuf\n" +
            "ACCORDING TO CALOPPA, WE AGREE TO THE FOLLOWING:\n" +
            "Users can visit our site anonymously. Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website. Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.\n" +
            "HOW DOES OUR SITE HANDLE DO NOT TRACK SIGNALS?\n" +
            "We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.\n" +
            "DOES OUR SITE ALLOW THIRD-PARTY BEHAVIORAL TRACKING?\n" +
            "It's also important to note that we allow third-party behavioral tracking\n" +
            "COPPA (CHILDREN ONLINE PRIVACY PROTECTION ACT)\n" +
            "When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.\n" +
            "We do not specifically market to children under the age of 13 years old.\n" +
            "FAIR INFORMATION PRACTICES\n" +
            "The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.\n" +
            "IN ORDER TO BE IN LINE WITH FAIR INFORMATION PRACTICES WE WILL TAKE THE FOLLOWING RESPONSIVE ACTION, SHOULD A DATA BREACH OCCUR:\n" +
            "We will notify you via email within 7 business day We will notify the users via in-site notification within 7 business day\n" +
            "We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non- compliance by data processors.\n" +
            "CAN SPAM ACT\n" +
            "The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.\n" +
            "WE COLLECT YOUR EMAIL ADDRESS IN ORDER TO:\n" +
            "Send information, respond to inquiries, and/or other requests or questions\n" +
            "Process orders and to send information and updates pertaining to orders.\n" +
            "Send you additional information related to your product and/or service\n" +
            "TO BE IN ACCORDANCE WITH CANSPAM, WE AGREE TO THE FOLLOWING:\n" +
            "Not use false or misleading subjects or email addresses.\n" +
            "Identify the message as an advertisement in some reasonable way.\n" +
            "Include the physical address of our business or site headquarters.\n" +
            "Monitor third-party email marketing services for compliance, if one is used.\n" +
            "       Honor opt-out/unsubscribe requests quickly.\n" +
            "Allow users to unsubscribe by using the link at the bottom of each email.\n" +
            "IF AT ANY TIME YOU WOULD LIKE TO UNSUBSCRIBE FROM RECEIVING FUTURE EMAILS, YOU CAN EMAIL US AT\n" +
            "Follow the instructions at the bottom of each email. and we will promptly remove you from ALL correspondence.\n" +
            "CONTACTING US:\n" +
            "If there are any questions regarding this privacy policy you may contact us using the information below:\n" +
            "www.hexakit.com\n" +
            "blvd \"Cherni vrah\" 47a, fl. 5\n" +
            "Sofia 1407\n" +
            "+ 1 (917) 214 7017\n" +
            "+ 359 885 362 007";

    //Terms Of Service Page
    String TERMS_OF_SERVING_HEADING = "TERMS OF SERVICE";
    String TERMS_OF_SERVING_TEXT = "TERMS OF SERVICE\n" +
            "OVERVIEW\n" +
            "This website is operated by Hexakit. Throughout the site, the terms “we”, “us” and “our” refer to Hexakit. Hexakit offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here. By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content. Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service. Any new features or tools which are added to the current website shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.\n" +
            "GENERAL CONDITIONS\n" +
            "We reserve the right to refuse service to anyone for any reason at any time. You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks. You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us. The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.\n" +
            "MODIFICATIONS TO THE SERVICE AND PRICES\n" +
            "Prices for our products are subject to change without notice. We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time. We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.\n" +
            "SECTION 5 - PRODUCTS OR SERVICES (IF APPLICABLE)\n" +
            "Hexakit services are available exclusively online through the website. We reserve the right, but are not obligated, to limit the sales of our Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the services that we offer.\n" +
            "ACCURACY OF BILLING AND ACCOUNT INFORMATION\n" +
            "You agree to provide current, complete and accurate purchase and account information for all purchases made at our website. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.\n" +
            "THIRD-PARTY LINKS\n" +
            "Certain content, products and services available via our Service may include materials from third-parties. Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties. We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party's policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party. USER COMMENTS, FEEDBACK AND OTHER SUBMISSIONS If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments. We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms of Service. You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.\n" +
            "PERSONAL INFORMATION\n" +
            "Your submission of personal information through the store is governed by our Privacy Policy.\n" +
            "PROHIBITED USES\n" +
            "In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.\n" +
            "DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY\n" +
            "We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free. In no case shall Hexakit, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service, or for any other claim related in any way to your use of the service, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content, posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.\n" +
            "INDEMNIFICATION\n" +
            "You agree to indemnify, defend and hold harmless Hexakit and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.\n" +
            "SEVERABILITY\n" +
            "In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.\n" +
            "TERMINATION\n" +
            "The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes. These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site. If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).\n" +
            "ENTIRE AGREEMENT\n" +
            "The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision. These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service). Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.\n" +
            "GOVERNING LAW\n" +
            "These Terms of Service and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of Bulgaria.\n" +
            "CHANGES TO TERMS OF SERVICE\n" +
            "You can review the most current version of the Terms of Service at any time at this page. We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.\n" +
            "CONTACT INFORMATION\n" +
            "Questions about the Terms of Service should be sent to us at:\n" +
            "\n" +
            "www.hexakit.com\n" +
            "blvd \"Cherni vrah\" 47a, fl. 5\n" +
            "Sofia 1407\n" +
            "+ 1 (917) 214 7017\n" +
            "+ 359 885 362 007";

}
