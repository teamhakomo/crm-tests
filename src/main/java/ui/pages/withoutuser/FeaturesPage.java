package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FeaturesPage implements Locators, Urls {

    @FindBy (id = FEATURES_PAGE_TEXTS)
    public static WebElement featuresContent;

    @FindBy (linkText = FEATURES_PRICING_BUTTON)
    public static WebElement pricingButton;

    public static void open () {
        Browser.driver.get(FEATURES);
        PageFactory.initElements(Browser.driver, FeaturesPage.class);
    }

    public static void isAt (String expectedTitle) {
        assertEquals("Does not match", expectedTitle, Browser.driver.getTitle());

    }

    public static void verifyTextContent (String expectedText) {
//        System.out.println(featuresContent.getText());
        assertTrue(featuresContent.getText().contains(expectedText));
        System.out.println("Verified feature text");

    }

    public static void goToPricingPage () {
        pricingButton.click();
    }


}
