package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrivacyPolicyPage implements Locators, Urls {

    @FindBy (xpath = PRIVACY_POLICY_HEADING)
    public static WebElement privacyPolicyHeading;

    @FindBy (id = PRIVACY_POLICY_TEXT_PAGE)
    public static WebElement privacyPolicyContent;

    @FindBy (linkText = PRIVACY_POLICY_HEXAKIT_LINK)
    public static WebElement hexakitLink;

    public static void open () {
        Browser.driver.get(PRIVACY_POLICY);
        PageFactory.initElements(Browser.driver, PrivacyPolicyPage.class);
    }

    public static void isAt (String expectedHeading) {
        assertEquals("Does not match", expectedHeading, privacyPolicyHeading.getText());
    }

    public static void verifyTextContent (String expectedText) {
//        System.out.println(privacyPolicyContent.getText());
        assertTrue(privacyPolicyContent.getText().contains(expectedText));
        System.out.println("Verified privacy policy");
    }
}
