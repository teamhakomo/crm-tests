package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PricingPage implements Locators, Urls {

    @FindBy (linkText = PRICING_SIGNUP_BUTTON)
    public static WebElement signupButton;

    @FindBy (linkText = PRICING_SIGNUP_LINK)
    public static WebElement signupLink;

    @FindBy (xpath = PRICING_TEXT_PAGE)
    public static WebElement pricingContent;

    public static void open () {
        Browser.driver.get(PRICING);
        PageFactory.initElements(Browser.driver, PricingPage.class);
    }

    public static void isAt (String expectedTitle) {
        assertEquals("Does not match", expectedTitle, Browser.driver.getTitle());
    }

    public static void goToSignupPageFromButton () {
        signupButton.click();
    }

    public static void goToSignupPageFromLink () {
        signupLink.click();
    }

    public static void verifyTextContent (String expectedText) {
//        System.out.println(pricingContent.getText());
        assertTrue(pricingContent.getText().contains(expectedText));
        System.out.println("Verified pricing text");
    }
}
