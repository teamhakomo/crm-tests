package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

/**
 * Created by Teodora on 7/3/17.
 */
public class LoginPage implements Locators, Urls {

    @FindBy (css = LOGIN_HEADING)
    public static WebElement loginHeading;

    @FindBy (id = LOGIN_EMAIL)
    public static WebElement emailInput;

    @FindBy (id = LOGIN_PASSWORD)
    public static WebElement passwordInput;

    @FindBy (xpath = LOGIN_BUTTON)
    public static WebElement loginButton;

    @FindBy (linkText = FORGOT_PASSWORD)
    public static WebElement forgotPassLink;

    @FindBy (id = FEEDBACK)
    public static WebElement feedbackButton;

    public static void openUrl () {
        Browser.driver.get(LOGIN);
        PageFactory.initElements(Browser.driver, LoginPage.class);
    }

    public static void open () {
        Homepage.open();
        Homepage.loginButton.click();
        PageFactory.initElements(Browser.driver, LoginPage.class);
    }


}
