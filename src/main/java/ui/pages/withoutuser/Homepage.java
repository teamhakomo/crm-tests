package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Teodora on 7/3/17.
 */
public class Homepage implements Urls, Locators {

    //Navbar
    @FindBy (className = NAV_LOGO)
    public static WebElement navLogo;

    @FindBy (linkText = NAV_HOME)
    public static WebElement homeLink;

    @FindBy (xpath = NAV_FEATURES)
    public static WebElement featuresNavLink;

    @FindBy (xpath = NAV_PRICING)
    public static WebElement pricingNavLink;

    @FindBy (linkText = NAV_LOGIN)
    public static WebElement loginButton;

    @FindBy (xpath = NAV_SIGNUP)
    public static WebElement signupNavButton;

    //Page
    @FindBy (xpath = HOME_SIGNUP)
    public static WebElement signupButton;

    @FindBy (className = HOME_VIDEO_SECTION)
    public static WebElement videoText;

    @FindBy (xpath = HOME_FEATURES_SECTION)
    public static WebElement featuresText;

    @FindBy (className = HOME_TESTIMONIALS_SECTION)
    public static WebElement testimonialsText;

    @FindBy (css = HOME_MOREINFO_SECTION)
    public static WebElement moreInfoText;

    @FindBy (className = HOME_PM_SECTION)
    public static WebElement pmText;

    //Footer
    @FindBy (xpath = FOOTER_FEATURES)
    public static WebElement featuresFooterLink;

    @FindBy (xpath = FOOTER_PRICING)
    public static WebElement pricingFooterLink;

    @FindBy (linkText = FOOTER_TEAM)
    public static WebElement teamLink;

    @FindBy (linkText = FOOTER_CONTACTS)
    public static WebElement contactsLink;

    @FindBy (linkText = FOOTER_FACEBOOK)
    public static WebElement facebookLink;

    @FindBy (linkText = FOOTER_TWITTER)
    public static WebElement twitterLink;

    @FindBy (linkText = FOOTER_LINKEDIN)
    public static WebElement linkedinLink;

    @FindBy (linkText = FOOTER_TERMS_OF_SERVICE)
    public static WebElement termsLink;

    @FindBy (linkText = FOOTER_PRIVACY_POLICY)
    public static WebElement privacyLink;

//    @FindBy(className = COPYRIGHT)
//    public static WebElement copyright;

    @FindBy (id = FEEDBACK)
    public static WebElement feedbackButton;

    public static void open () {
        Browser.driver.get(HOMEPAGE);
        PageFactory.initElements(Browser.driver, Homepage.class);
    }

    public static void isAt (String expectedHeading) {
        System.out.println(Browser.driver.getTitle());
        assertEquals("Does not match", expectedHeading, Browser.driver.getTitle());
    }

    public static void verifyVideoText (String expectedText) {
//        System.out.println(videoText.getText());
        assertTrue(videoText.getText().contains(expectedText));
        System.out.println("Verified video");
    }

    public static void verifyFeatureSectionText (String expectedText) {
//        System.out.println(featuresText.getText());
        assertTrue(featuresText.getText().contains(expectedText));
        System.out.println("Verified Feature");
    }

    public static void verifyTestimonialsSectionText (String expectedText) {
//        System.out.println(testimonialsText.getText());
        assertTrue(testimonialsText.getText().contains(expectedText));
        System.out.println("Verified Testimonials");
    }

    public static void verifyMoreInfoSectionText (String expectedText) {
//        System.out.println(moreInfoText.getText());
        assertTrue(moreInfoText.getText().contains(expectedText));
        System.out.println("Verified More Info");
    }

    public static void verifyPMSectionText (String expectedText) {
//        System.out.println(pmText.getText());
        assertTrue(pmText.getText().contains(expectedText));
        System.out.println("Verified PM Text");
    }

}