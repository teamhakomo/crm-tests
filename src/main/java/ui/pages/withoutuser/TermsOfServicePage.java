package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TermsOfServicePage implements Locators, Urls {

    @FindBy (xpath = TERMS_HEADING)
    public static WebElement termsHeading;

    @FindBy (id = TERMS_TEXT_PAGE)
    public static WebElement termsContent;

    @FindBy (linkText = TERMS_HEXAKIT_LINK)
    public static WebElement hexakitLink;

    public static void open () {
        Browser.driver.get(TERMS_OF_SERVICES);
        PageFactory.initElements(Browser.driver, TermsOfServicePage.class);
    }

    public static void isAt (String expectedHeading) {
        assertEquals("Does not match", expectedHeading, termsHeading.getText());
    }

    public static void verifyTextContent (String expectedText) {
//        System.out.println(termsContent.getText());
        assertTrue(termsContent.getText().contains(expectedText));
        System.out.println("Verified terms of service");
    }
}
