package ui.pages.withoutuser;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ui.pages.Locators;
import utils.Browser;
import utils.Urls;

public class SignupPage implements Locators, Urls {

    @FindBy (id = SIGNUP_FIRSTNAME)
    public static WebElement firstNameInput;

    @FindBy (id = SIGNUP_LASTNAME)
    public static WebElement lastNameInput;

    @FindBy (id = SIGNUP_EMAIL)
    public static WebElement emailInput;

    @FindBy (id = SIGNUP_PASSWORD)
    public static WebElement passInput;

    @FindBy (id = SIGNUP_CONFIRMPASS)
    public static WebElement confirmInput;

    @FindBy (xpath = SIGNUP_BUTTON)
    public static WebElement signupButton;

    @FindBy (id = FEEDBACK)
    public static WebElement feedbackButton;

    public static void openUrL () {
        Browser.driver.get(SIGNUP);
        PageFactory.initElements(Browser.driver, SignupPage.class);
    }

    public static void open () {
        Homepage.open();
        Homepage.signupButton.click();
        PageFactory.initElements(Browser.driver, SignupPage.class);
    }


}
